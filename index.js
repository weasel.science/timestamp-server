import net from 'net';

let clients = new Set;

const server = net.createServer((client) => {
  clients.add(client);
  console.log('Client connected');
  client.on('end', () => {
    console.log('Client disconnected');
    clients.delete(client);
  });
  client.on('error', () => {
    console.log('Client errored out');
    clients.delete(client);
  })
});

server.on('error', (err) => {
  console.log('Server error:', err);
  process.exit(1);
});

server.listen(10000, () => {
  console.log('Server bound to port 10000');
});

setInterval(() => {
  clients.forEach(client => {
    client.write((Date.now() / 1000).toFixed().toString());
    client.write('\n');
  });
}, 1000);